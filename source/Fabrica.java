import javax.swing.JOptionPane;
/**
	* Clase Fabrica
	* @author Daniel Vicente Arsego
	* @see Coche
	* @see swing.JOptionPane
	* @version 1.0
	*/
public class Fabrica {
	public static void main(String [] args) {
		Coche[] car = new Coche[Coche.max_coches];
		int index = 0;
		int cont = 0;
		int opcion = 0;
		do {
			try {
				opcion = Integer.parseInt(JOptionPane.showInputDialog("F\u00e1brica de Coches de Daniel Vicente Arsego. Selecciona una de las siguientes opciones:\n1. Fabricar coche(conociendo matricula)\n2. Fabricar coche(a partir del numero de puertas y el numero de plazas)\n3. Fabricar coche (a partir de la marca, el modelo y el color)\n4. Fabricar coche(sin datos)\n5. Tunear coche(pint\u00e1ndolo de un color)\n6. Tunear coche(sin pintarlo)\n7. Avanzar kil\u00f3metros\n8. Mostrar caracter\u00edsticas de un coche\n9. Salir del programa"));
				switch (opcion) {
					case 1:
						if (Coche.numCoches >= Coche.max_coches) {
							JOptionPane.showMessageDialog(null, "Ya no se puede crear mas coches");
							opcion = 10;
						}
						else {
							car[Coche.numCoches] = new Coche(JOptionPane.showInputDialog("Introduce la matricula:")); /** @see Coche#Coche(String aux1); */
							Coche.numCoches--;
							JOptionPane.showMessageDialog(null, caracteristicas(car[Coche.numCoches])); /** @see caracteristicas(); */
							Coche.numCoches++;
						}
						break;
					case 2:
						if (Coche.numCoches >= Coche.max_coches) {
							JOptionPane.showMessageDialog(null, "Ya no se puede crear mas coches");
							opcion = 10;
						}
						else {
							int numPuertas = Integer.parseInt(JOptionPane.showInputDialog(null, "Introduce el numero de puertas:"));
							int numPlazas = Integer.parseInt(JOptionPane.showInputDialog(null, "Introduce el numero de plazas:"));
							car[Coche.numCoches] = new Coche(numPuertas, numPlazas); /** @see Coche#Coche(int aux1, int aux2) ; */
							Coche.numCoches--;
							car[Coche.numCoches].setMatricula(matAleatoria()); /** @see matAleatoria(); */
							JOptionPane.showMessageDialog(null, caracteristicas(car[Coche.numCoches])); /** @see caracteristicas(); */
							Coche.numCoches++;
						}
						break;
					case 3:
						if (Coche.numCoches >= Coche.max_coches) {
							JOptionPane.showMessageDialog(null, "Ya no se puede crear mas coches");
							opcion = 10;
						}
						else {
							String marca = JOptionPane.showInputDialog(null, "Introduce la marca:");
							String modelo = JOptionPane.showInputDialog(null, "Introduce la modelo:");
							String color = JOptionPane.showInputDialog(null, "Introduce la color:");
							car[Coche.numCoches] = new Coche(marca, modelo, color);
							Coche.numCoches--;
							car[Coche.numCoches].matricular(matAleatoria()); /** @see matAleatoria(); */
							JOptionPane.showMessageDialog(null, caracteristicas(car[Coche.numCoches])); /** @see caracteristicas(); */
							Coche.numCoches++;
						}
						break;
					case 4:
						if (Coche.numCoches >= Coche.max_coches) {
							JOptionPane.showMessageDialog(null, "Ya no se puede crear mas coches");
							opcion = 10;
						}
						else {
							car[Coche.numCoches] = new Coche(); /** @see Coche#Coche(); */ 
							Coche.numCoches--;
							car[Coche.numCoches].setMatricula(matAleatoria()); /** @see matAleatoria(); */
							JOptionPane.showMessageDialog(null, caracteristicas(car[Coche.numCoches])); /** @see caracteristicas(); */
							Coche.numCoches++;
						}
						break;
					case 5:
						index = buscaCoche(car, JOptionPane.showInputDialog("Introduce la matricula:")); /** @see buscaCoche(); */
						if (index == -1) {
							JOptionPane.showMessageDialog(null, "No existe tal matricula");
							opcion = 10;
						}
						else {
							car[index].tunear(JOptionPane.showInputDialog("Elige el color:")); /** @see Coche#tunear(); */
							JOptionPane.showMessageDialog(null, caracteristicas(car[index])); /** @see caracteristicas(); */
						}
						break;
					case 6:
						index = buscaCoche(car, JOptionPane.showInputDialog("Introduce la matricula:")); /** @see buscaCoche(); */
						if (index == -1) {
							JOptionPane.showMessageDialog(null, "No existe tal matricula");
							opcion = 10;
						}
						else {
							car[index].tunear(); /** @see Coche#tunear(); */
							JOptionPane.showMessageDialog(null, caracteristicas(car[index])); /** @see caracteristicas(); */
						}
						break;
					case 7: 
						index = buscaCoche(car, JOptionPane.showInputDialog("Introduce la matricula:")); /** @see buscaCoche(); */
						if (index == -1) {
							JOptionPane.showMessageDialog(null, "No existe tal matricula");
							opcion = 10;
						}
						else {
							car[index].avanzar(Integer.parseInt(JOptionPane.showInputDialog("Introduce los KM:"))); /** @see Coche#avanzar(); */ 
							JOptionPane.showMessageDialog(null, caracteristicas(car[index])); /** @see caracteristicas(); */
						}
						break;
					case 8:
						index = buscaCoche(car, JOptionPane.showInputDialog("Introduce la matricula:")); /** @see buscaCoche(); */ 
						if (index == -1) {
							JOptionPane.showMessageDialog(null, "No existe tal matricula");
							opcion = 10;
						}
						else {
							JOptionPane.showMessageDialog(null, caracteristicas(car[index])); /** @see caracteristicas(); */
						}
						break;
					case 9:
						JOptionPane.showMessageDialog(null, "Estas saliendo...");
						break;
					default:
						JOptionPane.showMessageDialog(null, "Opcion incorrecta!");
						break;
				}		
				cont++;	
			}
			catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Esa opcion no existe");
				opcion = 10;
			}
				
		} while (opcion != 9);
		/**
		Metodo main de la fabrica de coche
		@return no tiene	
		*/
	}
	public static String caracteristicas(Coche car) {
		String techo;
		if (car.getTechoSolar() == false) {
			techo = "y no tiene techo solar";
		}
		else {
			techo = "y tiene techo solar";
		}
		String resultado = "Este coche tiene los siguientes datos:\n" + "Matricula: " + car.getMatricula() + "\nMarca: " + car.getMarca() + "\nModelo: " + car.getModelo() + "\nColor: " + car.getColor() + "\nKil\u00f3metros: " + car.getKm() + "\n" + techo + "\n" + "Tiene " + car.getPuertas() + " puertas y " + car.getPlazas() + " plazas.";
		return resultado;
		/**
		* @return resultado, devuelve las caracteristicas de un coche
		* @param car, es de tipo Coche	
		*/
	}
	public static String matAleatoria() {
		double numero1 = Math.random() * 100000;
		int numero2 = (int)numero1;
		String matriculaAle = Integer.toString(numero2);
		return matriculaAle;
		/** @return matriculaAle, devuelve una matricula aleatoria */
	}
	public static int buscaCoche(Coche[] car, String matricula) {
		int num = 0;
		int cont = 0;
		boolean resp = true;
		while (car[cont] != null && resp) {
			if (car[cont].getMatricula().equals(matricula)) {
				num = cont;
				resp = false;
			}
			else {
				num = -1;
				cont++;
			}
			if (cont == 5) {
				resp = false;
				cont--;
			}
		}			
		return num;
		/**
		* @return num, devuelve el indice de la posicion del array buscado
		* @param car y matricula, car es un array de tipo Coche y matricula es de tipo String	
		*/
	}
}