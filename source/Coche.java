import javax.swing.JOptionPane;
/**
* Clase Coche
* @author Daniel Vicente Arsego
* @version 1.0
*/
public class Coche {
	/** Variables de instancia de la clase */
	private String matricula;
	private String marca;
	private String modelo;
	private String color;
	private boolean techoSolar;
	private double kilometros;
	private int numPuertas;
	private int numPlazas;
	static int numCoches = 0;
	static final int max_coches = 5;
	
	public Coche() 
	{
		marca = "SEAT";
		modelo = "ALTEA";
		color = "blanco";
		techoSolar = false;
		kilometros = 0;
		numPuertas = 3;
		numPlazas = 5;
		numCoches++;
		/**Contructor por defecto */
	}
	public Coche(String aux1) 
	{
		matricula = aux1;
		marca = "SEAT";
		modelo = "ALTEA";
		color = "blanco";
		techoSolar = false;
		kilometros = 0;
		numPuertas = 3;
		numPlazas = 5;
		numCoches++;
		/**@param El parametro que recibe este constructor es igual a la matricula aux1 = matricula*/
	}
	public Coche(int aux1, int aux2) 
	{
		setPuertas(aux1);
		setPlazas(aux2);
		marca = "SEAT";
		modelo = "ALTEA";
		color = "blanco";
		techoSolar = false;
		kilometros = 0;
		numCoches++;
		/** @param Constructor aux1 = numeroPuertas y aux2 = numeroPlazas */
	}
	public Coche(String aux1, String aux2, String aux3) 
	{
		setMarca(aux1);
		setModelo(aux2);
		setColor(aux3);
		techoSolar = false;
		kilometros = 0;
		numPuertas = 3;
		numPlazas = 5;
		numCoches++;
		/** @param Constructor aux1 = marca aux2 = modelo aux3 = color */
	}
	public void setMatricula(String aux1) 
	{
		matricula = aux1;
		/** @param aux1 = matricula */
	}
	public String getMatricula() { 
		return matricula;
		/** @return devulve el valor de matricula(String) */
	}
	public void setMarca(String aux1) 
	{
		marca = aux1;
		/** @param aux1 = marca */
	}
	public String getMarca() { 
		return marca;
		/** @return devuelve el valor de marca (String) */
	}
	public void setModelo(String aux1) 
	{
		modelo = aux1;
		/** @param aux1 = modelo */
	}
	public String getModelo() { 
		return modelo;
		/** @return devuelve el valor de modelo (String) */
	}
	public void setColor(String aux1) 
	{
		color = aux1;
		/** @param aux1 = color */
	}
	public String getColor() { 
		return color;
		/** @return devuelve el valor de color (String) */
	}
	public void setTechoSolar(boolean aux1) 
	{
		techoSolar = aux1;
		/** @param aux1 = techoSolar */
	}
	public boolean getTechoSolar() 
	{ 
		return techoSolar;
		/** @return devuelve el valor de techoSolar (boolean) */
	}
	public void setKm(double aux1) 
	{
		if (aux1 > 0) {
			kilometros = aux1;
		}
		else {
			kilometros = 0;
		}
		/** @param aux1 = kilometros, comprueba si los KM introducidos son mayores que 0 */
	}
	public double getKm() { 
		return kilometros;
		/** @return devuelve el valor de km (double) */
	}
	public void setPuertas(int aux1) 
	{
		if (aux1 > 0 && aux1 <= 5) {
			numPuertas = aux1;
		}
		else {
			numPuertas = 3;
		}
		/** @param aux1 = numPuertas */
	}
	public int getPuertas() { 
		return numPuertas;
		/** @return devuelve el valor numPuertas (int) */
	}
	public void setPlazas(int aux1) 
	{
		if (aux1 > 0 && aux1 <= 7) {
			numPlazas = aux1;
		}
		else {
			numPlazas = 5;
		}
		/** @param aux1 = numPlazas */
	}
	public int getPlazas() { 
		return numPlazas;
		/** @return devuelve el valor de numPlazas (int) */
	}
	public void avanzar(double aux1) { 
		aux1 = kilometros + aux1;
		setKm(aux1);
		/** @param aux1 = kilometros, el usuario introduce los KM para que le sume a los que ya tenia */
	}
	public void tunear() { 
		kilometros = 0;
		techoSolar = true;
	}
	public void tunear(String aux1) { 
		setColor(aux1);
		kilometros = 0;
		techoSolar = true;
		/** @param color = aux1, el usuario introduce el nuevo color y añade un techoSolar al coche  */
	}
	public void matricular(String aux1) { 
		setMatricula(aux1);
		/** 
		* @param matricula = aux1
		*/ 
	}
}	
	